class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.integer :edad
      t.string :direccion
      t.string :ciudad
      t.string :telefono
      t.string :rol

      t.timestamps
    end
  end
end
