require "application_system_test_case"

class UsersTest < ApplicationSystemTestCase
  setup do
    @user = users(:one)
  end

  test "visiting the index" do
    visit users_url
    assert_selector "h1", text: "Users"
  end

  test "creating a User" do
    visit users_url
    click_on "Nuevo usuario"

    fill_in "Email", with: @user.email
    fill_in "Name", with: @user.name
    click_on "Crear usuario"

    assert_text "Usuario creado con éxito"
    click_on "Volver"
  end

  test "updating a User" do
    visit users_url
    click_on "Editar", match: :first

    fill_in "Email", with: @user.email
    fill_in "Name", with: @user.name
    click_on "Actualizar usuario"

    assert_text "Usuario actualizado con éxito"
    click_on "Volver"
  end

  test "destroying a User" do
    visit users_url
    page.accept_confirm do
      click_on "Eliminar", match: :first
    end

    assert_text "Usuario eliminado con éxito"
  end
end
