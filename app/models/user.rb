class User < ApplicationRecord

    validates :email, presence: true, format: /\w+@\w+\.{1}[a-zA-Z]{2,}/
    validates :nombre, :length => { :minimum => 3 }, :presence => true
    validates :edad, :length => { :minimum => 1, :maximum => 2 }, :presence => true
    validates :telefono, :length => { :minimum => 7, :maximum => 10 }, :presence => true

end
